#!/bin/bash
#  net=$(ls  -l /etc/sysconfig/network-scripts/ifcfg-*|awk   -F  "-" '{print  $NF}'|grep -v lo)
#  IP=$(for var in $net;do ifconfig $var 2>/dev/null;done|grep inet|grep -v  inet6|awk  '{print $2}')
IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."$3}')) ||IP=$(hostname -I |xargs -n 1   | grep  $(ip route |head  -n 1 | awk    '{print  $3}'  |  awk  -F  '.'  '{print  $1"."$2"."}'))

 SHELL_FOLDER=$(dirname "$0")
 cd  $SHELL_FOLDER;pwd

_coredns()  {
helm install  coredns   ./coredns-1.16.3.tgz   -n kube-system  --set "\
service.clusterIP=10.0.0.2,\
replicaCount=2,\
image.repository=coredns/coredns,\
image.tag=1.8.4,\
"
kubectl  run --image=busybox:1.28.4 -it  --rm --restart=Never  dns-test /bin/nslookup kubernetes
}
_ingress (){
helm install nginx-ingress-controller  -n kube-system  ./nginx-ingress-controller-7.6.21.tgz --set "
service.type=NodePort,\
service.nodePorts.http=32080,\
defaultBackend.service.type=NodePort,\
image.registry=docker.io,\
image.repository=bitnami/nginx-ingress-controller,\
image.tag=0.48.1-debian-10-r17,\
"
}
_prometheus() {
helm  install  monitor-prometheus    ./prometheus-14.7.0.tgz  --create-namespace -n monitor  --set "\
image.repository=quay.io/prometheus/alertmanager,\
image.tag=v0.21.0,\
configmapReload.prometheus.image.repository=jimmidyson/configmap-reload,\
configmapReload.prometheus.image.tag=v0.5.0,\
nodeExporter.image.repository=quay.io/prometheus/node-exporter,\
nodeExporter.image.tag=v1.1.2,\
pushgateway.image.repository=prom/pushgateway,\
pushgateway.image.tag=v1.3.1,\
alertmanager.image.repository=quay.io/prometheus/alertmanager,\
alertmanager.image.tag=v0.21.0,\
server.image.repository=quay.io/prometheus/prometheus,\
server.image.tag=v2.26.0,\
"
}
_grafana  (){
#https://artifacthub.io/packages/helm/bitnami/grafana
helm install   monitor-grafana  ./grafana-6.1.12.tgz   --create-namespace -n monitor  --set  "\
ingress.enabled=true,\
ingress.hostname=grafana.61.test.com,\
image.registry=swr.cn-southwest-2.myhuaweicloud.com,\
image.repository=bs-k8s/grafana,\
image.tag=8.1.3-debian-10-r0,\
service.type=NodePort,\
service.port=80,\
service.nodePort=30000,\
admin.password=admin,\
"  
}

_kuboard  (){
#https://kuboard.cn/install/v3/install-in-k8s.html#%E5%AE%89%E8%A3%85-2 
sed  "s/your-node-ip-address/$IP/g"  kuboard-v3-storage-class.yaml.bak   > kuboard-v3-storage-class.yaml
kubectl apply -f kuboard-v3-storage-class.yaml
echo  -e "
http://${IP}:30080
用户名： admin
密码： Kuboard123
"
}
_kubeapp () {
helm  install  kubeapps   ./kubeapps-7.5.2.tgz --create-namespace -n kubeapps  --set "\
frontend.service.type=NodePort,\
frontend.service.nodePort=30082,\
kubeappsapis.unsafeUseDemoSA=true,\
"
kubectl create --namespace default serviceaccount kubeapps-operator
kubectl create clusterrolebinding kubeapps-operator --clusterrole=cluster-admin --serviceaccount=default:kubeapps-operator

}
_kubesphere  (){
#https://kubesphere.io/zh/docs/quick-start/minimal-kubesphere-on-k8s/
kubectl apply -f https://github.com/kubesphere/ks-installer/releases/download/v3.1.1/kubesphere-installer.yaml
   
kubectl apply -f https://github.com/kubesphere/ks-installer/releases/download/v3.1.1/cluster-configuration.yaml
}
_coredns
_ingress
_prometheus
_grafana
_kuboard
_kubeapp